package Model;


public class Personne {
    public String nom;
    public int age;

    // constructeur vide (c'est le constructeur par défaut si aucun n'est défini)
    public Personne() {
    }

    public Personne(String nom, int age) {
        setNom(nom);
        // on appelle le setter pour bénéficier de la validation de l'âge
        setAge(age);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String value) {
        this.nom = value;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int value) {
         if (value < 18)
        throw new RuntimeException("too young!");
        this.age = value;
    }

    @Override
    public String toString() {
        return "Personne{" +
                "nom='" + nom + '\'' +
                ", age=" + age +
                '}';
    }
}