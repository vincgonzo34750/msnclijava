package Model;

public class Etudiant extends Personne {
    private int matricule;

    public Etudiant() {
    }

    public int getMatricule() {
        return matricule;
    }

    public void setMatricule(int value) {
        matricule = value;
    }

    @Override
    public String toString() {
        return "Etudiant{" +
                "nom='" + getNom() + '\'' +
                ", age=" + getAge() +
                ", matricule=" + matricule +
                '}';
    }
}