package Model;

public class Member {
    public Member() {
    }

    public Member(String pseudo) {
        this.pseudo = pseudo;
    }

    private String pseudo;

    @Override
    public String toString() {
        return "Member{" +
                "pseudo='" + pseudo + '\'' +
                '}';
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }
}
